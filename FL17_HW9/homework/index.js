class Employee {
    constructor(magazine) {
        this.magazine = magazine;
        this.magazine.addStaff(this)
    }
}

class MagazineEmployee {

    constructor(name, role, magazine) {
        this.name = name;
        this.role = role;
        this.magazine = magazine;
    }

    addArticle(name) {
        let article = name;
        let articles = this.magazine.articles;
        articles.add(article);
        if (articles.size >= 5) {
            this.magazine.state = this.magazine.state.next();
        }
    }

    approve() {
        if (this.role !== 'manager') {
            console.log('you do not have permissions to do it');
            return;
        }
    }
}

class ReadyForPublish {
    publish(magazine, name) {
        console.log(`Hello ${name} You've recently published publications.`)
        magazine(new PublishInProgress())
        const articles = magazine;
        const followers = magazine;
        articles.forEach(article => {
            followers.forEach(follower => {
                    follower(article.text);
                }
            )
        })
    }

    approve(magazine, name) {
        console.log(`Hello ${name} Publications have been already approved by you.`)
    }
}

class PublishInProgress {
}