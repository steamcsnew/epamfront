const getMaxEvenElement = arr => {
    const filterEven = arr.reduce((prev, curr) => {
        if (curr % 2 === 0) {
            prev.push(curr)
        }
        return [...prev];
    }, []);
    return Math.max(...filterEven);
}

let a = 3;
let b = 5;
[a, b] = [b, a];
console.log(a);//5
console.log(b);//3

function getValue(value) {
    return value ?? '-'
}

function getObjFromArray(arrayOfArrays) {
    let entries = new Map(arrayOfArrays)
    let obj = Object.fromEntries(entries)
    return obj
}

function addUniqueId(obj) {
    let {
        name: name
    } = obj;
    let id = Symbol()
    return {name, id}
}

function getRegroupedObject(oldObj) {
    let {
        name: firstName,
        details: {id, age, university}
    } = oldObj;

    let user = {age, firstName, id};
    return {university, user};
}

function getArrayWithUniqueElements(arr) {
    return [...new Set(arr)]
}

function hideNumber(numb) {
    numb = numb.replace(/\d(?=\d{4})/g, "*");
    return numb
}

function add(a = required('a'), b = required('b')) {
    return a + b
}

function required(m) {
    throw Error(m + ' is reqiured')
}

function* generateIterableSequence() {
    yield 'I';
    yield 'love';
    yield 'EPAM';
}




