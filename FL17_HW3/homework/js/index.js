'use strict';

/**
 * Class
 * @constructor
 * @param size - size of pizza
 * @param type - type of pizza
 * @throws {PizzaException} - in case of improper use
 */
let pizza;

function Pizza(size, type) {
    if (Pizza.allowedSizes.includes(size)) {
        if (Pizza.allowedTypes.includes(type)) {
            let obj = {
                size: size[0],
                price: size[1] + type[1],
                type: type[0],
                extra: []
            }
            console.log(obj)
            pizza = obj
        } else {
            throw new PizzaException('Invalid type')
        }
    } else {
        throw new PizzaException('Invalid size')
    }
}

function addExtraIngredient(extra) {
    if (!Pizza.allowedExtraIngredients.includes(extra)) {
        throw new PizzaException('Invalid ingredient')
    }
    if (pizza.extra.length === 0) {
        pizza.extra.push(extra[0])
        pizza.price += extra[1]
    } else {
        for (let i = 0; i < pizza.extra.length; i++) {
            if (pizza.extra[i] === extra[0]) {
                throw new PizzaException('Duplicate ingredient')
            }
        }
        pizza.extra.push(extra[0])
        pizza.price += extra[1]
    }
}

function removeExtraIngredient(extra) {
    for (let i = 0; i < pizza.extra.length; i++) {
        if (pizza.extra[i] === extra[0]) {
            pizza.extra.splice(i, 1)
        }
    }

}

function getSize() {
    return pizza.size
}

function getPrice() {
    return pizza.price
}

function getPizzaInfo() {
    return 'Size: ' + pizza.size + ',' + 'type:' + pizza.type + ';' +
        'extra ingredients:' + pizza.extra + ';' + 'price:' + pizza.price + 'UAH'
}

/* Sizes, types and extra ingredients */
//50
Pizza.SIZE_S = ['SMALL', 50]
//75
Pizza.SIZE_M = ['MEDIUM', 75]
//100
Pizza.SIZE_L = ['LARGE', 100]


//50
Pizza.TYPE_VEGGIE = ['VEGGIE', 50]
//60
Pizza.TYPE_MARGHERITA = ['MARGHERITA', 60]
//70
Pizza.TYPE_PEPPERONI = ['PEPPERONI', 70]


//7
Pizza.EXTRA_TOMATOES = ['TOMATOES', 7]
//5
Pizza.EXTRA_CHEESE = ['CHEESE', 5]
//9
Pizza.EXTRA_MEAT = ['MEAT', 9]

/* Allowed properties */
Pizza.allowedSizes = [Pizza.SIZE_S, Pizza.SIZE_M, Pizza.SIZE_L];
Pizza.allowedTypes = [Pizza.TYPE_VEGGIE, Pizza.TYPE_MARGHERITA, Pizza.TYPE_PEPPERONI]
Pizza.allowedExtraIngredients = [Pizza.EXTRA_TOMATOES, Pizza.EXTRA_CHEESE, Pizza.EXTRA_MEAT]


/**
 * Provides information about an error while working with a pizza.
 * details are stored in the log property.
 * @constructor
 */
function PizzaException(errorMessage) {
    this.log = errorMessage
}


/* It should work */
// // small pizza, type: veggie
// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// // add extra meat
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// // check price
// console.log(`Price: ${pizza.getPrice()} UAH`); //=> Price: 109 UAH
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_CHEESE);
// // add extra corn
// pizza.addExtraIngredient(Pizza.EXTRA_TOMATOES);
// // check price
// console.log(`Price with extra ingredients: ${pizza.getPrice()} UAH`); // Price: 121 UAH
// // check pizza size
// console.log(`Is pizza large: ${pizza.getSize() === Pizza.SIZE_L}`); //=> Is pizza large: false
// // remove extra ingredient
// pizza.removeExtraIngredient(Pizza.EXTRA_CHEESE);
// console.log(`Extra ingredients: ${pizza.getExtraIngredients().length}`); //=> Extra ingredients: 2
// console.log(pizza.getPizzaInfo()); //=> Size: SMALL, type: VEGGIE; extra ingredients: MEAT,TOMATOES; price: 114UAH.

// examples of errors
// let pizza = new Pizza(Pizza.SIZE_S); // => Required two arguments, given: 1

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.SIZE_S); // => Invalid type

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Duplicate ingredient

// let pizza = new Pizza(Pizza.SIZE_S, Pizza.TYPE_VEGGIE);
// pizza.addExtraIngredient(Pizza.EXTRA_MEAT); // => Invalid ingredient
