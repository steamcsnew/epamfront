let socket = new WebSocket("ws://localhost:8080");

socket.onopen = function () {
    let username = prompt('Введите свое имя:')
    let but = document.querySelector('button')
    but.addEventListener('click',
        function () {
            let input = document.querySelector('.inp').value
            let msg = document.querySelector('.message')
            let li = document.createElement('li')
            let br = document.createElement('br')
            let br2 = document.createElement('br')
            let message = document.createElement('span')
            message.setAttribute('class', 'mess')
            let time = document.createElement('span')
            let name = document.createElement('span')
            li.setAttribute('id', 'myMsg')
            message.innerHTML = input
            let Data = new Date();
            let Hour = Data.getHours();
            let Minutes = Data.getMinutes();
            let Seconds = Data.getSeconds();
            time.innerHTML = Hour + ':' + Minutes + ':' + Seconds
            name.innerHTML = username
            li.append(name, br, message, br2, time)
            msg.append(li)
            socket.send(username + '&' + input);
        })

};

socket.onmessage = function (event) {
    let str = event.data;
    let re = str.split("&");
    let msg = document.querySelector('.message')
    let li = document.createElement('li')
    let br = document.createElement('br')
    let br2 = document.createElement('br')
    let message = document.createElement('span')
    let time = document.createElement('span')
    let name = document.createElement('span')
    li.setAttribute('id', 'myMsg')
    message.innerHTML = re[1]
    let Data = new Date();
    let Hour = Data.getHours();
    let Minutes = Data.getMinutes();
    let Seconds = Data.getSeconds();
    time.innerHTML = Hour + ':' + Minutes + ':' + Seconds
    name.innerHTML = re[0]
    message.setAttribute('class', 'message2')
    time.setAttribute('class', 'mess2')
    name.setAttribute('class', 'mess2')
    li.append(name, br, message, br2, time)
    msg.append(li)
};

socket.onclose = function (event) {
    if (event.wasClean) {
        alert('Connection closed cleanly')
    } else {
        // e.g. server process killed or network down
        // event.code is usually 1006 in this case
        alert('Connection died');
    }
};

socket.onerror = function (error) {
    alert(error.message);
};