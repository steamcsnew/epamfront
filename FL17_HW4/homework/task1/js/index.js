// Your code goes here
fetch('https://jsonplaceholder.typicode.com/users')
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        appendData(data);
    })
    .catch(function (err) {
        console.log('error: ' + err);
    });

function appendData(data) {
    let mainContainer = document.getElementById("users");
    for (let i = 0; i < data.length; i++) {
        let li = document.createElement("li");
        let p = document.createElement("input")
        let button = document.createElement("button")
        let buttonDelete = document.createElement("button")
        button.setAttribute('type', 'submit')
        buttonDelete.setAttribute('type', 'submit')
        //button.onclick = edit();
        button.innerHTML = 'edit'
        buttonDelete.innerHTML = 'delete'
        p.classList.add('item');
        p.value = data[i].name;
        p.dataset.userId = data[i].id;
        button.dataset.userId = data[i].id;
        buttonDelete.dataset.userId = data[i].id;
        li.classList.add('item');
        li.dataset.userId = data[i].id;
        //li.addEventListener('click', (event) => getPosts(event))
        button.addEventListener('click', (event) => editUser(event))
        buttonDelete.addEventListener('click', (event) => deleteUser(event))
        li.append(p, button, buttonDelete)
        mainContainer.appendChild(li);
    }

}
let but = document.getElementsByTagName('button')
    //but.onclick = loadData()
const spinner = document.getElementById("spinner");

function loadData() {
    spinner.removeAttribute('hidden');
    fetch('https://www.mocky.io/v2/5185415ba171ea3a00704eed?mocky-delay=2000ms')
        .then(response => response.json())
        .then(data => {
            spinner.setAttribute('hidden', '');
            console.log(data)
        });
}
function cleanPosts() {
    let users = document.querySelectorAll('.item ul');
    for (let i = 0; i < users.length; i++) {
        if (users[i]) {
            users[i].style.display = 'none';
        }
    }
}

function editUser(event) {
    let userId = event.target.dataset.userId;
    fetch('https://jsonplaceholder.typicode.com/users/' + userId, {
        method: 'PUT',
        body: JSON.stringify({
            id:1,
            title:'foo',
            body:'bar',
            userId:1
        }),
        headers: {
            'Content-type': 'applications/json; charset=UTF-8'
        }
    }).then((response) => response.json())
        .then((json) => console.log(json))
}
function deleteUser(event){
    let userId = event.target.dataset.userId;
    fetch('https://jsonplaceholder.typicode.com/users/' + userId, {
        method: 'DELETE'
    })
}

// function getPosts(event) {
//
//     let userId = event.target.dataset.userId;
//
//     fetch(`https://jsonplaceholder.typicode.com/posts?userId=${userId}`)
//         .then(response => response.json())
//         .then(json => renderPosts(json, event.target))
// }

function renderPosts(posts, target) {
    let postsList = target.childNodes[1];

    cleanPosts();

    if (postsList) {
        postsList.style.display = 'block';
    } else {
        let list = document.createElement("ul");

        for (let i = 0; i < posts.length; i++) {

            let item = document.createElement("li");
            let liTitle = document.createElement("strong");
            let liBody = document.createElement("p");

            liTitle.innerHTML = posts[i].title;
            liBody.innerHTML = posts[i].body;

            item.appendChild(liTitle);
            item.appendChild(liBody);
            list.appendChild(item);
        }

        target.appendChild(list);
    }

}