let letsPlay = () => {
    document.querySelector('.game').removeAttribute('hidden')
    let btn = document.querySelector('.game-btn')
    btn.removeAttribute('hidden')
    let r = document.querySelector('.rules')
    r.setAttribute('hidden', true)
    let reset = document.querySelector('.reset')
    reset.removeAttribute('hidden')
}
let reset = () => {
    let p = document.querySelector('.result')
    p.innerHTML = ''
    let r = document.querySelector('.rules')
    r.removeAttribute('hidden')
    let reset = document.querySelector('.reset')
    reset.setAttribute('hidden', true)
}

let randomInteger = (min, max) => {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}
let you = 0;
let comp = 0;
let score = 0;
let game = (num) => {
    score = score + 1
    let list = ['Rock', 'Paper', 'Scissors']
    let userValue = num
    let progValue = randomInteger(0, 2)
    console.log(progValue)
    let p = document.querySelector('.result')
    let br = document.createElement('br')
    if (userValue === 1 && progValue === 0 ||
        userValue === 0 && progValue === 2 || userValue === 2 && progValue === 1) {
        you = you + 1
        p.append(`Round ${score}, ${list[userValue]} vs. ${list[progValue]}, You've WON!`, br)
    } else if (userValue === 0 && progValue === 1 ||
        userValue === 2 && progValue === 0 || userValue === 1 && progValue === 2) {
        comp = comp + 1
        p.append(`Round ${score}, ${list[userValue]} vs. ${list[progValue]}, You've LOST!`, br)
    } else {
        p.append(`Round ${score}, ${list[userValue]} vs. ${list[progValue]}, The friendship WON!`, br)
    }
    if (you === 3 || comp === 3) {
        if (you > comp) {
            p.append(br, "You've WON!")
        } else {
            p.append(br, "You've LOST!")
        }
        let but = document.querySelector('.game-btn')
        but.setAttribute('hidden', true)
    }
}
let reset2 = () => {
    you = 0;
    comp = 0;
    score = 0;
}
