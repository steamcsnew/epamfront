let letsPlay = () => {
    document.querySelector('.game').removeAttribute('hidden')
    let btn = document.querySelector('.game-btn')
    btn.removeAttribute('hidden')
    let r = document.querySelector('.rules')
    r.setAttribute('hidden', true)
    let reset = document.querySelector('.reset')
    reset.removeAttribute('hidden')
}
