let reset = () => {
    let p = document.querySelector('.result')
    p.innerHTML = ''
    let r = document.querySelector('.rules')
    r.removeAttribute('hidden')
    let reset = document.querySelector('.reset')
    reset.setAttribute('hidden', true)
}
