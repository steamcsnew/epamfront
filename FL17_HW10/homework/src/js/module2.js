
import * as app from './app'

export function removeTweet(tweetId) {
        let tweets = app.getTweets();
        tweets.splice(tweetId, 1)
     app.setTweets(tweets);
     app.mainPage()
    }

 export function errorAlert(str) {
        let hidden = document.querySelector('.hidden')
        hidden.style.display = 'block'
        let alert = document.querySelector('#alertMessageText')
        let time = function () {
            hidden.style.display = 'none'
        }
        alert.innerHTML = str;
        setTimeout(time, 2000)
    }
