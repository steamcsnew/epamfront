import * as module1 from './app'



    window.addEventListener('hashchange', function () {
    console.log(location.hash);
    if (/^#\/edit\/([0-9]+)$/.test(location.hash)) {
        let match = location.hash.match(/edit\/([0-9]+)/);
        module1.showEditPage(match[1]);
    }
    switch (location.hash) {
        case '':
        case '#':
            module1.mainPage();
            break;
        case'#/add':
            module1.addTweet();
            break;
        case '#/liked':
            module1.likedTweets();
            break;
    }
}, false)

   export function likedAlert(str) {
        let hidden = document.querySelector('.hidden')
        hidden.style.display = 'block'
        let alert = document.querySelector('#alertMessageText')
        let time = function () {
            hidden.style.display = 'none'
        }
        alert.innerHTML = str;
        setTimeout(time, 2000)

    }