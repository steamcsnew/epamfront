/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./js/app.js":
/*!*******************!*\
  !*** ./js/app.js ***!
  \*******************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"saveTweet\": () => (/* binding */ saveTweet),\n/* harmony export */   \"mainPage\": () => (/* binding */ mainPage),\n/* harmony export */   \"addTweet\": () => (/* binding */ addTweet),\n/* harmony export */   \"likedTweets\": () => (/* binding */ likedTweets),\n/* harmony export */   \"showEditPage\": () => (/* binding */ showEditPage),\n/* harmony export */   \"editTweet\": () => (/* binding */ editTweet),\n/* harmony export */   \"likeTweet\": () => (/* binding */ likeTweet),\n/* harmony export */   \"unlikeTweet\": () => (/* binding */ unlikeTweet),\n/* harmony export */   \"getTweets\": () => (/* binding */ getTweets),\n/* harmony export */   \"setTweets\": () => (/* binding */ setTweets),\n/* harmony export */   \"showEditPage2\": () => (/* binding */ showEditPage2)\n/* harmony export */ });\n/* harmony import */ var _module2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./module2 */ \"./js/module2.js\");\n/* harmony import */ var _module1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./module1 */ \"./js/module1.js\");\n/* harmony import */ var _scss_global_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../scss/global.scss */ \"./scss/global.scss\");\n\n\n\nvar btnAddTweet = document.querySelector('#addTweet');\nvar backBtn = document.createElement('button');\nbackBtn.innerHTML = 'back';\nbackBtn.setAttribute('onclick', 'history.back()');\nbackBtn.setAttribute('class', 'back');\nvar nav = document.querySelector('#navigationButtons');\nnav.append(backBtn);\nfunction saveTweet() {\n  var value = document.getElementById('modifyItemInput').value;\n  var tweets = getTweets();\n  var duplicated = false;\n  tweets.forEach(function (item) {\n    if (item.value === value || value.length > 140 || value.length < 0) {\n      duplicated = true;\n      return;\n    }\n  });\n\n  if (duplicated) {\n    _module2__WEBPACK_IMPORTED_MODULE_0__.errorAlert('Error! You can\\'t tweet about that');\n  } else {\n    var tweet = {\n      'value': value,\n      'liked': 0\n    };\n    tweets.push(tweet);\n    setTweets(tweets);\n    location.hash = '';\n  }\n}\nfunction mainPage() {\n  document.getElementsByTagName('h1')[0].innerHTML = 'Simple Twitter';\n  backBtn.style.display = 'none';\n  var main = document.querySelector('#tweetItems');\n  main.style.display = 'block';\n  var hide = document.querySelector('#modifyItem');\n  hide.style.display = 'none';\n  location.hash = '';\n  var list = document.querySelector('#list');\n  list.innerHTML = '';\n  var likedBtn = document.querySelector('.likeButton');\n\n  if (likedBtn !== null) {\n    likedBtn.remove();\n  }\n\n  var showLiked = 0;\n  getTweets().forEach(function (tweet, i) {\n    if (tweet.liked === 1) {\n      showLiked = 1;\n    }\n\n    var li = document.createElement('li');\n    li.innerHTML = tweet.value;\n    li.style.cursor = 'pointer';\n    li.setAttribute('id', i);\n    var btnRemove = document.createElement('button');\n    var btnLike = document.createElement('button');\n    btnRemove.setAttribute('class', 'remove');\n    btnLike.setAttribute('class', 'like');\n    btnRemove.innerHTML = 'remove';\n\n    if (localStorage.getItem('tweets') != null) {\n      btnRemove.setAttribute('onclick', \"removeTweet(\".concat(i, \")\"));\n\n      if (tweet.liked === 0) {\n        btnLike.innerHTML = 'like';\n        btnLike.setAttribute('onclick', \"likeTweet(\".concat(i, \")\"));\n      } else {\n        btnLike.innerHTML = 'unlike';\n        btnLike.setAttribute('onclick', \"unlikeTweet(\".concat(i, \")\"));\n      }\n    }\n\n    li.append(btnRemove, btnLike);\n    list.append(li);\n  });\n\n  if (showLiked) {\n    var liked = document.createElement('button');\n    liked.innerHTML = 'Go to liked';\n    liked.setAttribute('onclick', 'likedTweets()');\n    liked.setAttribute('class', 'likeButton');\n\n    var _nav = document.querySelector('#navigationButtons');\n\n    _nav.append(liked);\n  }\n}\nmainPage();\nfunction addTweet() {\n  var textarea = document.querySelector('#modifyItemInput');\n  textarea.value = '';\n  var show = document.querySelector('#modifyItem');\n  show.style.display = 'block';\n  var addTweetTextTitle = document.querySelector('#modifyItemHeader');\n  var hide = document.querySelector('#tweetItems');\n  addTweetTextTitle.innerHTML = 'Add tweet';\n  hide.style.display = 'none';\n  location.hash = '#/add';\n}\nfunction likedTweets() {\n  document.getElementsByTagName('h1')[0].innerHTML = 'Liked Tweets';\n  var addTweet = document.querySelector('.addTweet');\n  var likeButton = document.querySelector('.likeButton');\n  likeButton.style.display = 'none';\n  addTweet.style.display = 'none';\n  var hide = document.querySelector('#modifyItem');\n  hide.style.display = 'none';\n  location.hash = '#/liked';\n  var list = document.querySelector('#list');\n  list.innerHTML = '';\n  backBtn.style.display = 'block\t';\n  getTweets().forEach(function (tweet, i) {\n    if (tweet.liked === 1) {\n      var li = document.createElement('li');\n      li.setAttribute('id', i);\n      li.innerHTML = tweet.value;\n      var btnRemove = document.createElement('button');\n      var btnLike = document.createElement('button');\n      btnRemove.innerHTML = 'remove';\n      btnLike.setAttribute('class', 'like');\n      btnRemove.setAttribute('class', 'remove');\n\n      if (localStorage.getItem('tweets') != null) {\n        btnRemove.setAttribute('onclick', \"removeTweet(\".concat(i, \")\"));\n\n        if (tweet.liked === 0) {\n          btnLike.innerHTML = 'like';\n          btnLike.setAttribute('onclick', \"likeTweet(\".concat(i, \")\"));\n        } else {\n          btnLike.innerHTML = 'unlike';\n          btnLike.setAttribute('onclick', \"unlikeTweet(\".concat(i, \")\"));\n        }\n      }\n\n      li.style.cursor = 'pointer';\n      li.append(btnRemove, btnLike);\n      list.append(li);\n    }\n  });\n}\naddEventListener('click', function (e) {\n  if (e.target.tagName === 'LI') {\n    location.href = '#/edit/' + e.target.id;\n    showEditPage(e.target.id);\n  }\n\n  if (e.target.id === 'cancelModification') {\n    history.back();\n  }\n});\nfunction showEditPage(tweetId) {\n  var tweets = getTweets();\n  var tweet = tweets[tweetId];\n  var textarea = document.querySelector('#modifyItemInput');\n  textarea.value = tweet.value;\n  showEditPage2(tweetId);\n  var show = document.querySelector('#modifyItem');\n  var hide = document.querySelector('#tweetItems');\n  hide.style.display = 'none';\n  show.style.display = 'block';\n  show.style.cursor = 'pointer';\n  var addTweetTextTitle = document.querySelector('#modifyItemHeader');\n  addTweetTextTitle.innerHTML = 'Edit Tweet';\n}\nfunction editTweet(tweetId) {\n  var value = document.getElementById('modifyItemInput').value;\n  var tweets = getTweets();\n  var duplicated = false;\n  tweets.forEach(function (item) {\n    if (item.value === value || value.length > 140 || value.length < 0) {\n      duplicated = true;\n      return;\n    }\n  });\n\n  if (duplicated) {\n    _module2__WEBPACK_IMPORTED_MODULE_0__.errorAlert('Error! You can\\'t tweet about that');\n  } else {\n    tweets[tweetId].value = value;\n    setTweets(tweets);\n    location.hash = '';\n    mainPage();\n  }\n}\nfunction likeTweet(tweetId) {\n  var tweets = getTweets();\n  tweets[tweetId].liked = 1;\n  _module1__WEBPACK_IMPORTED_MODULE_1__.likedAlert('Hooray! You liked tweet with id ' + tweetId + '!');\n  setTweets(tweets);\n  mainPage();\n}\nfunction unlikeTweet(tweetId) {\n  var tweets = getTweets();\n  tweets[tweetId].liked = 0;\n  _module1__WEBPACK_IMPORTED_MODULE_1__.likedAlert('Sorry you no longer like tweet with id ' + tweetId);\n  setTweets(tweets);\n  mainPage();\n}\nfunction getTweets() {\n  var tweets = [];\n  var data = localStorage.getItem('tweets');\n\n  if (data !== null) {\n    tweets = JSON.parse(data);\n  }\n\n  return tweets;\n}\nfunction setTweets(tweets) {\n  localStorage.setItem('tweets', JSON.stringify(tweets));\n}\n\nbtnAddTweet.onclick = function () {\n  addTweet();\n};\n\nvar saveTweetBtn = document.querySelector('#saveModifiedItem');\n\nsaveTweetBtn.onclick = function () {\n  saveTweet();\n};\n\nfunction showEditPage2(id) {\n  var saveTweetBtn = document.querySelector('#saveModifiedItem');\n\n  saveTweetBtn.onclick = function () {\n    editTweet(id);\n  };\n}\n\n//# sourceURL=webpack://homework/./js/app.js?");

/***/ }),

/***/ "./js/module1.js":
/*!***********************!*\
  !*** ./js/module1.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"likedAlert\": () => (/* binding */ likedAlert)\n/* harmony export */ });\n/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app */ \"./js/app.js\");\n\nwindow.addEventListener('hashchange', function () {\n  console.log(location.hash);\n\n  if (/^#\\/edit\\/([0-9]+)$/.test(location.hash)) {\n    var match = location.hash.match(/edit\\/([0-9]+)/);\n    _app__WEBPACK_IMPORTED_MODULE_0__.showEditPage(match[1]);\n  }\n\n  switch (location.hash) {\n    case '':\n    case '#':\n      _app__WEBPACK_IMPORTED_MODULE_0__.mainPage();\n      break;\n\n    case '#/add':\n      _app__WEBPACK_IMPORTED_MODULE_0__.addTweet();\n      break;\n\n    case '#/liked':\n      _app__WEBPACK_IMPORTED_MODULE_0__.likedTweets();\n      break;\n  }\n}, false);\nfunction likedAlert(str) {\n  var hidden = document.querySelector('.hidden');\n  hidden.style.display = 'block';\n  var alert = document.querySelector('#alertMessageText');\n\n  var time = function time() {\n    hidden.style.display = 'none';\n  };\n\n  alert.innerHTML = str;\n  setTimeout(time, 2000);\n}\n\n//# sourceURL=webpack://homework/./js/module1.js?");

/***/ }),

/***/ "./js/module2.js":
/*!***********************!*\
  !*** ./js/module2.js ***!
  \***********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"removeTweet\": () => (/* binding */ removeTweet),\n/* harmony export */   \"errorAlert\": () => (/* binding */ errorAlert)\n/* harmony export */ });\n/* harmony import */ var _app__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app */ \"./js/app.js\");\n/* harmony import */ var _module1__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./module1 */ \"./js/module1.js\");\n\n\nfunction removeTweet(tweetId) {\n  var tweets = _app__WEBPACK_IMPORTED_MODULE_0__.getTweets();\n  tweets.splice(tweetId, 1);\n  _app__WEBPACK_IMPORTED_MODULE_0__.setTweets(tweets);\n  _app__WEBPACK_IMPORTED_MODULE_0__.mainPage();\n}\nfunction errorAlert(str) {\n  var hidden = document.querySelector('.hidden');\n  hidden.style.display = 'block';\n  var alert = document.querySelector('#alertMessageText');\n\n  var time = function time() {\n    hidden.style.display = 'none';\n  };\n\n  alert.innerHTML = str;\n  setTimeout(time, 2000);\n}\n\n//# sourceURL=webpack://homework/./js/module2.js?");

/***/ }),

/***/ "./scss/global.scss":
/*!**************************!*\
  !*** ./scss/global.scss ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n\n\n//# sourceURL=webpack://homework/./scss/global.scss?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./js/app.js");
/******/ 	
/******/ })()
;