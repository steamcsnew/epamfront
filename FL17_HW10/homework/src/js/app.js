import * as module2 from"./module2";
import * as module1 from"./module1";
import '../scss/global.scss'
let btnAddTweet = document.querySelector('#addTweet');
let backBtn = document.createElement('button')
backBtn.innerHTML = 'back'
backBtn.setAttribute('onclick', 'history.back()')
backBtn.setAttribute('class', 'back')
let nav = document.querySelector('#navigationButtons');
nav.append(backBtn)
export function saveTweet() {
    let value = document.getElementById('modifyItemInput').value
    let tweets = getTweets();
    let duplicated = false;
    tweets.forEach(function (item) {
        if (item.value === value || value.length > 140 || value.length < 0) {
            duplicated = true;
            return;
        }
    });
    if (duplicated) {
        module2.errorAlert('Error! You can\'t tweet about that')
    } else {
        let tweet = {
            'value': value,
            'liked': 0
        }
        tweets.push(tweet);
        setTweets(tweets);
        location.hash = ''
    }
}
export function mainPage() {
    document.getElementsByTagName('h1')[0].innerHTML = 'Simple Twitter'
    backBtn.style.display = 'none';
    let main = document.querySelector('#tweetItems');
    main.style.display = 'block';

    let hide = document.querySelector('#modifyItem');
    hide.style.display = 'none'
    location.hash = ''
    let list = document.querySelector('#list');
    list.innerHTML = ''
    let likedBtn = document.querySelector('.likeButton');
    if (likedBtn !== null) {
        likedBtn.remove();
    }

    let showLiked = 0;
    getTweets().forEach(function (tweet, i) {
        if (tweet.liked === 1) {
            showLiked = 1;
        }
        let li = document.createElement('li')
        li.innerHTML = tweet.value;
        li.style.cursor = 'pointer'
        li.setAttribute('id', i)
        let btnRemove = document.createElement('button')
        let btnLike = document.createElement('button')
        btnRemove.setAttribute('class','remove')
        btnLike.setAttribute('class','like')
        btnRemove.innerHTML = 'remove'
        if(localStorage.getItem('tweets') !== null) {
            btnRemove.setAttribute('onclick',`removeTweet(${i})`)
            if (tweet.liked === 0) {
                btnLike.innerHTML = 'like'
                btnLike.setAttribute('onclick',`likeTweet(${i})`)
            } else {
                btnLike.innerHTML = 'unlike'
                btnLike.setAttribute('onclick',`unlikeTweet(${i})`)
            }
        }
        li.append(btnRemove, btnLike)
        list.append(li)

    })
    if (showLiked) {
        let liked = document.createElement('button')
        liked.innerHTML = 'Go to liked'
        liked.setAttribute('onclick', 'likedTweets()')
        liked.setAttribute('class', 'likeButton')
        let nav = document.querySelector('#navigationButtons');
        nav.append(liked)
    }

}

mainPage()


export function addTweet() {
    let textarea = document.querySelector('#modifyItemInput');
    textarea.value = '';
    let show = document.querySelector('#modifyItem');
    show.style.display = 'block'
    let addTweetTextTitle = document.querySelector('#modifyItemHeader');
    let hide = document.querySelector('#tweetItems');
    addTweetTextTitle.innerHTML = 'Add tweet'
    hide.style.display = 'none'
    location.hash = '#/add'

}




export function likedTweets() {
    document.getElementsByTagName('h1')[0].innerHTML = 'Liked Tweets'
    let addTweet = document.querySelector('.addTweet');
    let likeButton = document.querySelector('.likeButton');
    likeButton.style.display = 'none'
    addTweet.style.display = 'none'
    let hide = document.querySelector('#modifyItem');
    hide.style.display = 'none'
    location.hash = '#/liked'
    let list = document.querySelector('#list');
    list.innerHTML = ''
    backBtn.style.display = 'block	';

    getTweets().forEach(function (tweet, i) {
        if (tweet.liked === 1) {
            let li = document.createElement('li')
            li.setAttribute('id', i)
            li.innerHTML = tweet.value;
            let btnRemove = document.createElement('button')

            let btnLike = document.createElement('button')
            btnRemove.innerHTML = 'remove'
            btnLike.setAttribute('class','like')
            btnRemove.setAttribute('class','remove')
            if(localStorage.getItem('tweets') !== null) {
                btnRemove.setAttribute('onclick',`removeTweet(${i})`)
                if (tweet.liked === 0) {
                    btnLike.innerHTML = 'like'
                    btnLike.setAttribute('onclick',`likeTweet(${i})`)
                } else {
                    btnLike.innerHTML = 'unlike'
                    btnLike.setAttribute('onclick',`unlikeTweet(${i})`)
                }
            }

            li.style.cursor = 'pointer'
            li.append(btnRemove, btnLike)
            list.append(li)
        }
    })
}

addEventListener('click', function (e) {
    if (e.target.tagName === 'LI') {
        location.href = '#/edit/' + e.target.id
        showEditPage(e.target.id);
    }
    if (e.target.id === 'cancelModification') {
        history.back();
    }
});

export function showEditPage(tweetId) {
    let tweets = getTweets();
    let tweet = tweets[tweetId];
    let textarea = document.querySelector('#modifyItemInput');
    textarea.value = tweet.value;

    showEditPage2(tweetId)
    let show = document.querySelector('#modifyItem');
    let hide = document.querySelector('#tweetItems');
    hide.style.display = 'none'
    show.style.display = 'block'
    show.style.cursor = 'pointer'
    let addTweetTextTitle = document.querySelector('#modifyItemHeader');
    addTweetTextTitle.innerHTML = 'Edit Tweet'
}

export function editTweet(tweetId) {
    let value = document.getElementById('modifyItemInput').value;
    let tweets = getTweets();
    let duplicated = false;
    tweets.forEach(function (item) {
        if (item.value === value || value.length > 140 || value.length < 0) {
            duplicated = true;
            return;
        }
    });
    if (duplicated) {
        module2.errorAlert('Error! You can\'t tweet about that')
    } else {
        tweets[tweetId].value = value;
        setTweets(tweets);
        location.hash = ''
        mainPage()
    }
}

export function likeTweet(tweetId) {
    let tweets = getTweets();
    tweets[tweetId].liked = 1;
    module1.likedAlert('Hooray! You liked tweet with id ' + tweetId + '!')
    setTweets(tweets);
    mainPage()
}

export function unlikeTweet(tweetId) {
    let tweets = getTweets();
    tweets[tweetId].liked = 0;
    module1.likedAlert('Sorry you no longer like tweet with id ' + tweetId)
    setTweets(tweets);
    mainPage()
}

export function getTweets() {
    let tweets = [];
    let data = localStorage.getItem('tweets');
    if (data !== null) {
        tweets = JSON.parse(data);
    }
    return tweets;
}

 export function setTweets(tweets) {
    localStorage.setItem('tweets', JSON.stringify(tweets));
}
btnAddTweet.onclick= function () {
    addTweet()
}
let saveTweetBtn = document.querySelector('#saveModifiedItem');
saveTweetBtn.onclick= function () {
    saveTweet()
}
export function showEditPage2 (id) {
    let saveTweetBtn = document.querySelector('#saveModifiedItem');
    saveTweetBtn.onclick= function () {
        editTweet(id)
    }
}





