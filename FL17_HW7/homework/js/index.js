$(document).ready(function () {

    $(document).on('mouseenter', '.btn-x', function () {
        $(this).find(':button').show();
    }).on('mouseleave', '.btn-x', function () {
        $(this).find(':button').hide();
    }).on('click', ':button.btn-x', function () {
        $(this).parent().remove();

    });

    $(document).on('mouseenter', '.circle', function () {
        $(this).find(':button').show();
    }).on('mouseleave', '.circle', function () {
        $(this).find(':button').hide();
    }).on('click', ':button.circle', function () {
        $(this).css('background-color', 'red')
        $(this).toggleClass('inactive')
    });

    function calculator() {
        let sum = '';
        let len;
        let inputVal = document.getElementById('screen');
        $('.buttons .digit').on('click', function () {
            let num = $(this).attr('value');
            if (sum.toString().slice(-1) === '/' && num === '0') {
                let err = document.createElement('span')
                err.innerHTML = 'ERROR';
                err.style.color = 'red';
                $('#screen').html(err);
                throw new Error()
            } else {
                sum += num;
                $('#screen').html(sum);
                len = inputVal.innerHTML.split('');

            }
        });
        $('.buttons .operator').on('click', function (e) {
            e.preventDefault();
            if (sum.toString().slice(-1) === '+' || sum.toString().slice(-1) === '-'
                || sum.toString().slice(-1) === '*' || sum.toString().slice(-1) === '/') {
                let ops = $(this).attr('value');
                sum = sum.replace(/.$/, ops)
                $('#screen').html(sum);
                len = inputVal.innerHTML;
                if (/(?=(\D{2}))/g.test(sum)) {
                    sum = len.substring(0, len.length - 1);
                    $('#screen').html(sum);
                }
            } else {
                let ops = $(this).attr('value');
                sum += ops;
                $('#screen').html(sum);
                len = inputVal.innerHTML;
                if (/(?=(\D{2}))/g.test(sum)) {
                    sum = len.substring(0, len.length - 1);
                    $('#screen').html(sum);
                }

            }

        });


        $('#equal').on('click', function () {
            let total = (sum) => {
                return new Function('',`return(${sum})`)()
            }
            let tot = total(sum) % 1 !== 0 ? total(sum).toFixed(2) : total(sum)
            $('#screen').html(tot);
            let li = document.createElement('li')
            li.setAttribute('class', 'list')
            let span = document.createElement('span')
            let circle = document.createElement('button')
            circle.setAttribute('class', 'circle inactive')
            let button = document.createElement('button')
            button.setAttribute('class', 'btn-x')
            button.innerHTML = 'X'
            span.innerHTML = sum + ' = ' + tot
            if (sum.includes('48') || tot.toString().includes('48')) {
                span.style.textDecoration = 'underline'
            }
            li.append(circle, span, button)
            $(function () {
                $('.main-log').prepend(li)
            });
        });

        $('#clear').on('click', function () {
            sum = '';
            $('#screen').html(0);
        });

    }
    calculator();
    $('.main-log').scroll(function () {
        const scrolled = $('.main-log').scrollTop()
        console.log('Scroll top: ' + scrolled)
    });


});
